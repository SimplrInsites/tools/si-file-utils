//! A file byte reader using tokio that returns strings. This is for things like files containing
//! json, csv records, or another text format
//!
//! ---
//! author: Andrew Evans
//! ---

use std::io::SeekFrom;
use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};

use encoding_rs::Decoder;
use tokio::fs::File;
use tokio::io::AsyncReadExt;
use tokio::sync::mpsc::{self, Receiver, Sender};
use tokio::sync::Semaphore;

const BUFFERSIZE: usize = 5096;


/// Bytes reader for a file
pub struct ByteReader{
    path: String,
    decoder: Option<Decoder>,
    cache_size: i32,
    offset: u64,
    sender: Sender<Option<String>>,
    queue_sema: Arc<Semaphore>,
    is_complete: Arc<AtomicBool>
}


impl ByteReader{

    /// Check the cache size. Cannot be reset
    #[allow(dead_code)]
    pub fn get_cache_size(&mut self) -> i32{
        self.cache_size.clone()
    }

    /// Get all the bytes concatenated
    ///
    /// # Arguments
    /// * `bytes` - Bytes to be appended to current_cache
    /// * `existing_bytes`
    fn get_all_bytes(bytes: &[u8], current_cache: Vec<u8>) -> Vec<u8>{
        let mut out_bytes = current_cache.clone();
        for b in bytes{
            out_bytes.push(b.clone())
        }
        out_bytes
    }

    /// Check if the byte array contains a windows terminator
    ///
    /// # Arguments
    /// * `barr` - Byte array to check
    fn is_linux_terminator(barr: &[u8; 2]) -> bool{
        let linux = "\n".as_bytes();
        let wb = linux.get(0).unwrap();
        let pb = barr.get(0).unwrap();
        if wb != pb{
            false
        }else{
            true
        }
    }

    /// Check if the byte array contains a windows terminator
    ///
    /// # Arguments
    /// * `barr` - Byte array to check
    fn is_windows_terminator(barr: &[u8; 2]) -> bool{
        let windows = "\r\n".as_bytes();
        let mut is_terminator = true;
        for i in 0..windows.len(){
            let wb = windows.get(i).unwrap();
            let pb = barr.get(i).unwrap();
            if wb != pb{
                is_terminator = false;
                break;
            }
        }
        is_terminator
    }

    /// Update the last two array
    ///
    /// # Arguments
    /// * `barr` - Byte array containing last two bytes read
    /// * `b` - byte
    fn update_last_two(mut barr: [u8; 2], b: u8) -> [u8;2]{
        let tmp = barr[0].clone();
        barr[1] = tmp;
        barr[0] = b;
        barr
    }

    /// Get any lines. Return a vector of strings and a vector of bytes
    ///
    /// # Arguments
    /// * `byte_vec` - Byte vector to search within
    fn look_for_lines(byte_vec: Vec<u8>) -> (Vec<String>, Vec<u8>){
        let mut lines = Vec::<String>::new();
        let mut last_two = [0u8; 2];
        let mut i = 0;
        let mut cache = Vec::<u8>::new();
        while i < byte_vec.len(){
            let b = byte_vec.get(i).unwrap();
            last_two = ByteReader::update_last_two(last_two, b.clone());
            cache.push(b.clone());
            if i > 1 && ByteReader::is_windows_terminator(&last_two){
                let mut ocache = cache.clone();
                let _r = ocache.remove(ocache.len() - 1);
                let _r = ocache.remove(ocache.len() -1);
                let line_result = String::from_utf8(ocache);
                cache = Vec::<u8>::new();
                last_two = [0u8; 2];
                if line_result.is_ok() {
                    let line = line_result.ok().unwrap();
                    lines.push(line);
                }
            }else if ByteReader::is_linux_terminator(&last_two){
                let mut ocache = cache.clone();
                let _r = ocache.remove(ocache.len() - 1);
                let line_result = String::from_utf8(ocache);
                cache = Vec::<u8>::new();
                last_two = [0u8; 2];
                if line_result.is_ok() {
                    let line = line_result.ok().unwrap();
                    lines.push(line);
                }
            }
            i += 1;
        }
        (lines, cache)
    }

    /// Get the lines and an updated cache
    ///
    /// # Arguments
    /// * `bytes` - Most recently read lines and cache
    /// * `current_cache` - Cache entering the function
    fn get_lines_and_cache(bytes: &[u8], current_cache: Vec<u8>) -> (Vec<String>, Vec<u8>){
        let byte_vec = ByteReader::get_all_bytes(bytes, current_cache);
        let (lines, new_cache) = ByteReader::look_for_lines(byte_vec);
        (lines, new_cache)
    }

    /// Read the next line or None if at end of file
    pub async fn read_lines(&mut self){
        let offset = self.offset.clone();
        let mut sender = self.sender.clone();
        let mut lines = Vec::<String>::new();
        let mut byte_cache = Vec::<u8>::new();
        let file_result = File::open(self.path.clone()).await;
        let mut file = file_result.ok().unwrap();
        if offset > 0{
            let _r = file.seek(SeekFrom::Start(offset as u64)).await;
        }
        let meta = file.metadata().await;
        let fsize = meta.unwrap().len();
        let mut remaining = fsize.clone() as usize;
        let mut decoder = self.decoder.take();
        while remaining > 0 || lines.len() > 0{
            if lines.len() == 0 {
                let bytes_read = remaining.min(BUFFERSIZE);
                let mut buf = [0u8; BUFFERSIZE];
                let _r = file.read_exact(&mut buf).await;
                if let Some(mut d) = decoder.take(){
                    let mut dst = [0u8; BUFFERSIZE];
                    let _r = d.decode_to_utf8(&buf, &mut dst, false);
                    buf = dst;
                }
                let (new_lines, new_byte_cache) = ByteReader::get_lines_and_cache(
                    &buf, byte_cache);
                byte_cache = new_byte_cache;
                lines = new_lines;
                remaining = remaining - bytes_read;
            }else{
                let str = lines.pop().unwrap();
                let _r = sender.send(Some(str)).await;
                self.queue_sema.add_permits(1);
            }
        }
        if byte_cache.is_empty() == false{
            let s = String::from_utf8(byte_cache);
            if s.is_ok(){
                let _r = sender.send(Some(s.ok().unwrap())).await;
                self.queue_sema.add_permits(1);
            }
        }
        let _r = sender.send(None).await;
        self.queue_sema.add_permits(1);
        self.is_complete.store(true, Ordering::Relaxed)

    }

    /// Create a new bytereader
    ///
    /// # Arguments
    /// * `path` - Path to the file
    /// * `cache_size` - Size of the cache
    /// * `decoder` - UTF-8 decoder
    /// * `encoding` - Encoding type
    pub fn new(
        path: String,
        cache_size: Option<i32>,
        decoder: Option<Decoder>,
        offset: Option<u64>) -> (ByteReader, Receiver<Option<String>>, Arc<Semaphore>){
        let mut store_size = 10;
        let mut osize = 0;
        if offset.is_some(){
            osize = offset.unwrap();
        }
        if cache_size.is_some(){
            store_size = cache_size.unwrap();
        }
        let sema = Arc::new(Semaphore::new(0));
        let (sender, receiver) = mpsc::channel(store_size as usize);
        let reader = ByteReader{
            path,
            queue_sema: sema.clone(),
            cache_size: store_size,
            sender,
            is_complete: Arc::new(AtomicBool::new(false)),
            decoder,
            offset: osize
        };
        (reader, receiver, sema)
    }
}


#[cfg(test)]
pub mod tests{
    use tokio::runtime::Runtime;

    use super::*;

    fn get_fpath() -> &'static str{
        let fpath = env!("file_test_path");
        fpath
    }

    fn get_runtime() -> Runtime{
        let rt = tokio::runtime::Builder::new()
            .enable_all()
            .core_threads(4)
            .threaded_scheduler()
            .enable_all()
            .build();
        rt.ok().unwrap()
    }

    #[test]
    fn should_read_string_bytes(){
        let fpath = get_fpath();
        let (mut br, mut rec, _qsema) = ByteReader::new(
            fpath.to_string(),Some(10), None, Some(120));
        let mut rt = get_runtime();
        let h = rt.spawn(async move{
            br.read_lines().await;
            let completed = br.is_complete.load(Ordering::Relaxed);
            assert!(completed);
        });
        rt.block_on(async move{
            let mut lines: i32 = 0;
            let mut run = true;
            while run {
                let l = rec.recv().await;
                if l.is_some() {
                    let s = l.unwrap();
                    if s.is_some() {
                        lines += 1;
                    }
                }else{
                    run = false;
                }
            }
            let _r = h.await;
            assert_eq!(lines, 74);
        });
    }
}
