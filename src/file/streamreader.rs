//! A streaming line reader. Call create_iterators to obtain objects to read from
//! and a handle that needs to stay active while in use. Call close when complete from
//! this module.
//!
//! ---
//! author: Andrew Evans
//! ---

use std::sync::{Arc, Condvar, Mutex};
use std::sync::atomic::AtomicBool;

use encoding_rs::Decoder;
use tokio::sync;

use crate::file::bytes::ByteReader;
use crate::file::iterator::LineIterator;

/// A file based line reader
pub struct FileLineReader{
    path: String,
}


/// File Line Readder implementation
impl FileLineReader{

    /// Create new File Reader
    ///
    /// # Arguments
    /// *`path` - Path to the file to use
    pub fn new(path: String) -> FileLineReader{
        FileLineReader{
            path
        }
    }
}


/// Create an iterator and a handle. Where the iterator is for the user and the handle
/// performs reading.
///
/// # Arguments
/// * `reader` - FileLineReader
/// * `cache_size` - Size of the cache
/// * `decoder` - Decoder, if any, to convert bytes to utf-8
/// * `offset` - Initial file offset
pub fn create_iterator(
    reader: FileLineReader,
    cache_size: Option<i32>,
    decoder: Option<Decoder>,
    offset: Option<u64>) -> (ByteReader, LineIterator){
    let (br, r, qsema) =  ByteReader::new(
        reader.path, cache_size, decoder, offset);
    let receiver_arc = Arc::new(sync::Mutex::new(r));
    let cvar = Arc::new(
        (Mutex::new(AtomicBool::new(true)), Condvar::new()));
    let line_it = LineIterator::new(
        receiver_arc.clone(), cvar.clone(), qsema);
    (br, line_it)
}


#[cfg(test)]
pub mod test{
    use tokio::runtime::Runtime;

    use super::*;

    fn get_fpath() -> &'static str{
        env!("file_test_path")
    }

    fn get_runtime() -> Runtime{
        let rt = tokio::runtime::Builder::new()
            .enable_all()
            .core_threads(4)
            .threaded_scheduler()
            .enable_all().build();
        rt.unwrap()
    }

    #[test]
    fn should_build_reader(){
        let mut rt = get_runtime();
        rt.block_on(async move {
            let path = get_fpath();
            let fr = FileLineReader::new(path.to_string());
            let (mut read, mut it) = create_iterator(
                fr, Some(10), None, None);
            let h = tokio::spawn(async move {
                read.read_lines().await;
            });
            let r = it.next().await;
            assert!(r.is_ok());
            let stropt = r.ok().unwrap();
            assert!(stropt.is_some());
            it.close().await;
            let _r = h.await;
        });
    }

    #[test]
    fn should_read_to_end(){
        let mut rt = get_runtime();
        rt.block_on(async move {
            let path = get_fpath();
            println!("{}", path);
            let fr = FileLineReader::new(path.to_string());
            let (mut read, mut it) = create_iterator(
                fr, Some(10), None, None);
            let h = tokio::spawn(async move {
                read.read_lines().await;
            });
            let mut i = 0;
            let mut run = true;
            while run {
                let r = it.next().await;
                assert!(r.is_ok());
                let stropt = r.ok().unwrap();
                if stropt.is_none(){
                    run = false;
                }else{
                    i += 1;
                }
            }
            assert_eq!(i, 75);
            it.close().await;
            let _r = h.await;
        });
    }
}
