//! A stream writer. Writes bytes to a file.
//!
//! ---
//! author: Andrew Evans
//! ---

use tokio::fs::OpenOptions;
use tokio::sync::mpsc::{self, Sender, Receiver};
use tokio::io::AsyncWriteExt;
use tokio::task::JoinHandle;
use std::sync::atomic::{AtomicBool, AtomicUsize, Ordering};
use std::sync::{Arc, Condvar, Mutex};
use std::time::Duration;


/// Writer to write to the stream.
#[derive(Clone, Debug)]
pub struct Writer{
    sender: Sender<Vec<u8>>,
    write_count: Arc<AtomicUsize>
}


impl Writer{

    /// Sends bytes to be written to the buffer and waits for the file write.
    pub async fn write_bytes(&mut self, bytes: Vec<u8>){
        let mut sender = self.sender.clone();
        let r = sender.send(bytes).await;
        if r.is_ok(){
            self.write_count.fetch_add(1, Ordering::SeqCst);
        }
    }

    /// Create a new writer from the sender
    ///
    /// # Arguments
    /// * `sender` - The byte vector sender
    /// * `write_count` - Keeps track of number of writes to perform
    pub fn new(
        sender: Sender<Vec<u8>>,
        write_count: Arc<AtomicUsize>) -> Writer{
        Writer{
            sender,
            write_count
        }
    }
}


/// File Stream writer implementation
pub struct FileStreamHandler{
    handle: Option<JoinHandle<()>>,
    status: Arc<AtomicBool>,
    writer: Writer,
    write_count: Arc<AtomicUsize>
}


/// File Stream Writer implementation
impl FileStreamHandler{

    /// Start the stream writer
    async fn start_writer(
        mut receiver: Receiver<Vec<u8>>,
        status: Arc<AtomicBool>,
        append: bool,
        path: &'static str,
        cvar: Arc<(Mutex<bool>, Condvar)>,
        write_count: Arc<AtomicUsize>){
        let file_result = OpenOptions::new()
            .write(!append)
            .append(append)
            .create(true)
            .open(path)
            .await;
        if file_result.is_ok(){
            let mut f = file_result.ok().unwrap();
            {
                let &(ref _m, ref condvar) = &*cvar.clone();
                condvar.notify_all();
            }
            let mut run = true;
            while run || write_count.load(Ordering::SeqCst) > 0{
                run = status.load(Ordering::Relaxed);
                let msg_result = receiver.recv().await;
                if msg_result.is_some(){
                    write_count.fetch_sub(1, Ordering::SeqCst);
                    let mut msg = msg_result.unwrap();
                    let slice = msg.as_mut_slice();
                    let _r = f.write_all(slice).await;
                }
            }
        }
        receiver.close();
    }

    /// Close the file writer. Force close forces the handle to stop writing
    /// to the file immediately rather than waiting for lines to finish.
    ///
    /// # Arguments
    /// * `force` - Whether to force close immediately
    pub async fn close(&mut self, force: bool){
        if force{
            self.write_count.store(0, Ordering::SeqCst);
        }
        self.status.store(false, Ordering::Relaxed);
        let handle = self.handle.take();
        if let Some(handle) = handle{
            let _r = handle.await;
        }
    }

    /// Create a writer by cloning the internal writer
    pub fn create_writer(&self) -> Writer{
        self.writer.clone()
    }

    /// Create a new File Stream Writer
    ///
    /// # Arguments
    /// * `path` - Path to the file
    /// * `append` - Whether to append to the file
    /// * `cache_size` - Size of the cache
    pub async fn new(
        path: &'static str,
        append: bool,
        cache_size: usize) -> FileStreamHandler{
        let write_count = Arc::new(AtomicUsize::new(0));
        let (sender, receiver) = mpsc::channel(cache_size);
        let status = Arc::new(AtomicBool::new(true));
        let control = status.clone();
        let cvar_pair = Arc::new((Mutex::new(false), Condvar::new()));
        let fsvar = cvar_pair.clone();
        let twc = write_count.clone();
        let handle = tokio::spawn(async move{
            FileStreamHandler::start_writer(
                receiver,
                status.clone(),
                append,
                path,
                fsvar,
                twc).await;
        });
        let writer = Writer::new(
            sender, write_count.clone());
        let &(ref m, ref cvar) = &*cvar_pair.clone();
        let mlock = m.lock();
        if mlock.is_ok() {
            let mgaurd = mlock.ok().unwrap();
            let d = Duration::from_millis(10000);
            let r = cvar.wait_timeout(mgaurd, d);
            assert!(r.is_ok());
            let fsw = FileStreamHandler {
                handle: Some(handle),
                status: control,
                writer,
                write_count
            };
            fsw
        }else{
            panic!("Failed to Wait for File");
        }
    }
}


#[cfg(test)]
pub mod tests{
    use super::*;
    use tokio::runtime::Runtime;

    fn get_runtime() -> Runtime{
        let rt = tokio::runtime::Builder::new()
            .enable_all()
            .core_threads(4)
            .threaded_scheduler()
            .enable_all().build();
        rt.unwrap()
    }

    fn get_file_path() -> &'static str{
        env!("write_file_test_path")
    }

    fn delete_test_file() {
        let fpath = get_file_path();
        let _r = std::fs::remove_file(fpath);
    }

    fn read_test_string() -> String{
        let fpath = get_file_path();
        let file_result = std::fs::read_to_string(fpath);
        file_result.ok().unwrap()
    }

    #[test]
    fn should_create_stream_writer(){
        let mut rt = get_runtime();
        let fpath = get_file_path();
        rt.block_on(async move {
            let mut fw = FileStreamHandler::new(
                fpath, true, 15).await;
            fw.close(true).await;
        });
    }

    #[test]
    fn should_write_to_file(){
        let mut rt = get_runtime();
        let fpath = get_file_path();
        delete_test_file();
        let _r = rt.block_on(async move {
            let fw = FileStreamHandler::new(
                fpath, true, 15).await;
            let str = "Hello World!\n".as_bytes().to_vec();
            let mut writer = fw.create_writer();
            writer.write_bytes(str.clone()).await;
            writer.write_bytes(str.clone()).await;
            let contents = read_test_string();
            println!("Content: {}", contents);
            assert!(contents.eq("Hello World!\nHello World!\n"));
        });
        delete_test_file();
    }

    #[test]
    fn should_write_to_file_concurrently(){
        let mut rt = get_runtime();
        let fpath = get_file_path();
        for _i in 0..100 {
            delete_test_file();
            let _r = rt.block_on(async move {
                let mut fw = FileStreamHandler::new(
                    fpath, true, 15).await;
                let mut writer = fw.create_writer();
                let mut writer2 = fw.create_writer();
                let handle1 = tokio::spawn(async move {
                    let str = "Hello World!\n".as_bytes().to_vec();
                    writer.write_bytes(str.clone()).await;
                    writer.write_bytes(str.clone()).await;
                    writer.write_bytes(str.clone()).await;
                });
                let handle2 = tokio::spawn(async move {
                    let str = "Hello World!\n".as_bytes().to_vec();
                    writer2.write_bytes(str.clone()).await;
                    writer2.write_bytes(str.clone()).await;
                    writer2.write_bytes(str.clone()).await;
                    writer2.write_bytes(str.clone()).await;
                });
                let _r = handle1.await;
                let _r = handle2.await;
                fw.close(false).await;
            });
            let contents = read_test_string();
            assert!(contents.contains("Hello World!\n"));
            let lines = contents.lines().collect::<Vec<&str>>();
            assert_eq!(lines.len(), 7);
        }
        delete_test_file();
    }
}
