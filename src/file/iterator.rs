//! Iterator wrapped around hte stealer that communicates with the handler.
//!
//! ---
//! author: Andrew Evans
//! ---

use tokio::sync::{self, mpsc, Semaphore};
use tokio::sync::mpsc::Receiver;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::{Arc, Condvar, Mutex};
use std::time::Duration;
use tokio::sync::mpsc::error::TryRecvError;


/// Line Iterator exposing a public API and return lines from the file.
pub struct LineIterator{
    receiver: Arc<sync::Mutex<Receiver<Option<String>>>>,
    condvar: Arc<(Mutex<AtomicBool>, Condvar)>,
    queue_sema: Arc<Semaphore>
}


impl LineIterator {
    /// Steal from the stealer. Will block until the next is available.
    async fn get_next(&mut self) -> Result<Option<String>, TryRecvError> {
        let receiver = self.receiver.clone();
        let mut m = receiver.lock().await;
        let permit = self.queue_sema.acquire().await;
        permit.forget();
        let r = m.recv().await;
        if r.is_some(){
            Ok(r.unwrap())
        }else{
            Ok(None)
        }
    }

    /// Get the result timeout. It is ok to timeout. In that case the response is an empty option
    ///
    /// # Arguments
    /// * `cvar` - Conditional variable
    /// * `in_mgaurd` - Mutex gaurd to wait on
    /// * `timeout` - Time to wait for
    async fn get_result_timeout<'a>(
        &mut self,
        timeout: Option<u64>) -> Result<Option<String>, TryRecvError> {
        let mut err = true;
        {
            let &(ref m, ref cvar) = &*self.condvar.clone();
            let mlock = m.lock();
            let mgaurd = mlock.ok().unwrap();
            let d = Duration::from_millis(timeout.unwrap());
            let r = cvar.wait_timeout(mgaurd, d);
            if r.is_ok() {
                let (mgaurd, wait_result) = r.ok().unwrap();
                if wait_result.timed_out() == false && (*mgaurd).load(Ordering::Relaxed) {
                    err = true
                }
            }
        }
        if !err {
            self.get_next().await
        } else {
            Ok(None)
        }
    }

    /// Attempt to get a result with try next before running other functions
    async fn try_get(&mut self) -> Result<Option<String>, TryRecvError> {
        let mut lock_result = self.receiver.lock().await;
        let pr = self.queue_sema.try_acquire();
        let r = lock_result.try_recv();
        if r.is_ok() {
            if pr.is_ok(){
                let permit = pr.unwrap();
                permit.forget();
            }
            Ok(r.ok().unwrap())
        } else {
            Ok(None)
        }
    }

    /// Wait for a result when nothing was present but the receive did not fail.
    ///
    /// # Arguments
    /// * `timeout` - Timeout for the receive
    async fn handle_no_result(&mut self, timeout: Option<u64>) -> Result<Option<String>, TryRecvError> {
        let cpair = self.condvar.clone();
        let mut b = true;
        let mut err = false;
        {
            let (m, _cvar) = &*cpair;
            let lresult = m.lock();
            if lresult.is_ok() {
                {
                    let mgaurd = lresult.ok().unwrap();
                    b = (&*mgaurd).load(Ordering::Relaxed);
                }
            } else {
                err = true;
            }
        }
        if !err {
            if b {
                if timeout.is_some() {
                    self.get_result_timeout(timeout).await
                } else {
                    self.next().await
                }
            } else {
                self.try_get().await
            }
        } else {
            Err(TryRecvError::Empty)
        }
    }

    /// Wait until data is available. This will fail on the first receive failure.
    ///
    /// # Argument
    /// * `timeout` - Timeout to await on
    pub async fn wait(&mut self, timeout: Option<u64>) -> Result<Option<String>, TryRecvError> {
        let r = self.try_get().await;
        if r.is_ok() {
            let m = r.ok().unwrap();
            if m.is_some() {
                Ok(m)
            }else{
                self.handle_no_result(timeout).await
            }
        }else{
            Err(TryRecvError::Empty)
        }
    }

    /// Try to get the next value from the channel without waiting.
    pub async fn try_next(&mut self) -> Result<Option<String>, ()> {
        let mut mr = self.receiver.lock().await;
        let pr = self.queue_sema.try_acquire();
        let r = mr.try_recv();
        if r.is_ok() {
            if pr.is_ok(){
                let permit = pr.unwrap();
                permit.forget();
            }
            let s = r.ok().unwrap();
            if s.is_some() {
                Ok(s)
            } else {
                Ok(None)
            }
        } else {
            Ok(None)
        }
    }

    /// Get the next result or none if it does not exist. An empty channel returns None.
    pub async fn next(&mut self) -> Result<Option<String>, TryRecvError> {
        self.get_next().await
    }

    /// Close the iterator. Please call this
    pub async fn close(&mut self){
        let mut m = self.receiver.lock().await;
        m.close();
    }

    /// Create a new Line Iterator
    ///
    /// # Arguments
    /// * `receiver` - Message receiver
    /// * `condvar` - The conditional variable to wait on
    /// * `queue_sema` - Semaphore to wait on queue
    pub fn new(
        receiver: Arc<sync::Mutex<Receiver<Option<String>>>>,
        condvar: Arc<(Mutex<AtomicBool>, Condvar)>,
        queue_sema: Arc<Semaphore>) -> LineIterator {
        LineIterator {
            receiver,
            condvar,
            queue_sema
        }
    }
}


#[cfg(test)]
pub mod test{
    use super::*;
    use tokio;
    use tokio::runtime::Runtime;

    fn get_runtime() -> Runtime{
        let rt = tokio::runtime::Builder::new()
            .enable_all()
            .core_threads(4)
            .threaded_scheduler()
            .enable_all().build();
        rt.unwrap()
    }

    #[test]
    fn should_create_iterator(){
        let (_sender, receiver) = mpsc::channel(100);
        let qsema = Arc::new(Semaphore::new(0));
        let _it = LineIterator{
            receiver: Arc::new(sync::Mutex::new(receiver)),
            condvar: Arc::new((Mutex::new(AtomicBool::new(true)), Condvar::new())),
            queue_sema: qsema
        };
    }

    #[test]
    fn should_wait_no_timeout(){
        let mut rt = get_runtime();
        let (mut sender, receiver) = mpsc::channel(100);
        let qsema = Arc::new(Semaphore::new(0));
        let hsema = qsema.clone();
        let h = rt.spawn(async move{
            for _i in 0..100 as i32{
                let _r = sender.send(Some("test".to_string())).await;
                hsema.add_permits(1);
            }
        });
        rt.block_on(async move{
            let mut it = LineIterator{
                receiver: Arc::new(sync::Mutex::new(receiver)),
                condvar: Arc::new((Mutex::new(AtomicBool::new(true)), Condvar::new())),
                queue_sema: qsema
            };
            let _r = h.await;
            let r = it.wait(None).await;
            let opt = r.ok().unwrap();
            assert!(opt.is_some());
            assert!(opt.unwrap().eq("test"));
        })
    }

    #[test]
    fn should_wait_with_timeout(){
        let mut rt = get_runtime();
        let (mut sender, receiver) = mpsc::channel(100);
        let qsema = Arc::new(Semaphore::new(0));
        let hsema = qsema.clone();
        let h = rt.spawn(async move{
            for _i in 0..100 as i32{
                let _r = sender.send(Some("test".to_string())).await;
                hsema.add_permits(1);
            }
        });
        rt.block_on(async move{
            let mut it = LineIterator{
                receiver: Arc::new(sync::Mutex::new(receiver)),
                condvar: Arc::new((Mutex::new(AtomicBool::new(true)), Condvar::new())),
                queue_sema: qsema
            };
            let _r = h.await;
            let _r = it.wait(None).await;
            let r = it.wait(None).await;
            let opt = r.ok().unwrap();
            assert!(opt.is_some());
            assert!(opt.unwrap().eq("test"));
        })
    }

    #[test]
    fn should_get_empty_on_wait_no_timeout(){
        let mut rt = get_runtime();
        let (mut _sender, receiver) = mpsc::channel(100);
        rt.block_on(async move{
            let qsema = Arc::new(Semaphore::new(0));
            let mut it = LineIterator{
                receiver: Arc::new(sync::Mutex::new(receiver)),
                condvar: Arc::new((Mutex::new(AtomicBool::new(true)), Condvar::new())),
                queue_sema: qsema
            };
            let r = it.wait(None).await;
            let opt = r.ok().unwrap();
            assert!(opt.is_none());
        })
    }

    #[test]
    fn should_get_empty_on_wait_timeout(){
        let mut rt = get_runtime();
        let (mut _sender, receiver) = mpsc::channel(100);
        rt.block_on(async move{
            let qsema = Arc::new(Semaphore::new(0));
            let mut it = LineIterator{
                receiver: Arc::new(sync::Mutex::new(receiver)),
                condvar: Arc::new((Mutex::new(AtomicBool::new(true)), Condvar::new())),
                queue_sema: qsema
            };
            let r = it.wait(Some(1)).await;
            let opt = r.ok().unwrap();
            assert!(opt.is_none());
        })
    }

    #[test]
    fn should_get_next(){
        let mut rt = get_runtime();
        let (mut sender, receiver) = mpsc::channel(100);
        let qsema = Arc::new(Semaphore::new(0));
        let hsema = qsema.clone();
        let h = rt.spawn(async move{
            for _i in 0..1 as i32{
                let _r = sender.send(Some("test".to_string())).await;
                hsema.add_permits(1);
            }
        });
        rt.block_on(async move{
            let mut it = LineIterator{
                receiver: Arc::new(sync::Mutex::new(receiver)),
                condvar: Arc::new((Mutex::new(AtomicBool::new(true)), Condvar::new())),
                queue_sema: qsema
            };
            let _r = h.await;
            let r = it.next().await;
            let opt = r.ok().unwrap();
            assert!(opt.is_some());
            assert!(opt.unwrap().eq("test"));
        })
    }
}
