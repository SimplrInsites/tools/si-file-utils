//! CSV reader. Returns String data to be converted later due to usage needs.
//!
//! ---
//! author: Andrew Evans
//! ---

use std::collections::HashMap;

use csv::{Reader, ReaderBuilder, StringRecord};
use encoding_rs::Decoder;
use serde::{Serialize, Deserialize};
use tokio::task::JoinHandle;

use crate::file::bytes::ByteReader;
use crate::file::iterator::LineIterator;
use crate::file::streamreader::{self, FileLineReader};

type Record = HashMap<String, String>;


/// CSV Options
#[derive(Builder, Clone, Serialize, Deserialize)]
pub struct CSVReaderOptions{
    headers: Vec<String>,
    #[builder(default = "self.get_default_delimiter()")]
    pub delimiter: u8,
    #[builder(default = "100")]
    pub batch_size: u8,
    #[builder(default = "false")]
    pub double_quote: bool,
    #[builder(default = "self.get_none_u8()")]
    pub escape: Option<u8>,
    #[builder(default = "self.get_none_i32()")]
    pub cache_size: Option<i32>,
    #[builder(default = "self.get_none_u64()")]
    pub offset: Option<u64>
}


impl CSVReaderOptionsBuilder{

    /// Get the default delimiter, a comma
    fn get_default_delimiter(&self) -> u8{
        ",".as_bytes()[0]
    }

    /// Get the builder option default, none.
    fn get_none_u8(&self) -> Option<u8>{
        None
    }

    /// Get the builder option default, none.
    fn get_none_i32(&self) -> Option<i32>{
        None
    }

    /// Get the builder option default, none.
    fn get_none_u64(&self) -> Option<u64>{
        None
    }
}


/// CSV Reader for the application.
#[allow(dead_code)]
pub struct CSVReader{
    headers: StringRecord,
    options: CSVReaderOptions,
    byte_handle: Option<JoinHandle<()>>,
    line_iterator: LineIterator
}


/// Implementation of the CSV Reader
impl CSVReader{

    /// Get the next batch from the reader
    async fn get_batch(&mut self) -> Vec<String>{
        let options = self.options.clone();
        let mut batch_vec = vec![];
        let bsize = options.batch_size;
        for _i in 0..bsize {
            let it_result = self.line_iterator.next().await;
            if it_result.is_ok(){
                let it_opt = it_result.unwrap();
                if it_opt.is_some(){
                    let record = it_opt.unwrap();
                    batch_vec.push(record);
                }else{
                    break;
                }
            }
        }
        batch_vec
    }

    /// Package the batch from the reader
    ///
    /// # Arguments
    /// * `reader` - Reader to obtain records from
    fn package_batch(&mut self, mut reader: Reader<&[u8]>) -> Vec<HashMap<String, String>>{
        let mut records: Vec<HashMap<String, String>> = vec![];
        let it = reader.deserialize();
        for result in it{
            if result.is_ok(){
                let record: Record = result.ok().unwrap();
                records.push(record);
            }
        }
        records
    }

    /// Get the next record if it exists. Returns None on empty. An empty
    /// vector means that processing may have failed.
    pub async fn next(&mut self) -> Option<Vec<HashMap<String, String>>>{
        let options = self.options.clone();
        let headers = self.headers.clone();
        let batch_vec = self.get_batch().await;
        if batch_vec.is_empty() == false {
            let batch_str = batch_vec.join("\n");
            let batch_str_ref = batch_str.as_str();
            let mut reader: Reader<&[u8]> =  ReaderBuilder::new()
                .delimiter(options.delimiter.clone())
                .has_headers(true)
                .double_quote(options.double_quote.clone())
                .escape(options.escape.clone()).from_reader(batch_str_ref.as_bytes());
            reader.set_headers(headers);
            let records= self.package_batch(reader);
            Some(records)
        }else{
            None
        }
    }

    /// Create the iterator and bytereader for the underlying file
    ///
    /// # Arguments
    /// * `path` - Path to the fiule
    /// * `options` - CSV options
    /// * `decoder` - Optional decoder
    fn get_iterator(path: String,
                    options: &CSVReaderOptions,
                    decoder: Option<Decoder>) -> (ByteReader, LineIterator){
        let reader = FileLineReader::new(path.to_string());
        streamreader::create_iterator(
            reader, options.cache_size, decoder, options.offset)
    }

    /// Convert the headers to a string record
    ///
    /// # Arguments
    /// * `headers` - Convert headers to a string record for writing
    fn vec_to_string_record(headers: Vec<String>) -> StringRecord{
        StringRecord::from(headers)
    }

    /// Close the csv reader
    pub async fn close(&mut self){
        self.line_iterator.close().await;
        let handle_opt = self.byte_handle.take();
        if let Some(handle) = handle_opt{
            let _r = handle.await;
        }
    }

    /// Create a new CSV Reader
    ///
    /// # Arguments
    /// * `path` - Path to the csv
    /// * `decoder` - Optional decoder
    pub async fn new(
        path: String,
        options: CSVReaderOptions,
        decoder: Option<Decoder>) -> CSVReader{
        let (mut byte_reader, line_iterator) = CSVReader::get_iterator(
            path, &options, decoder);
        let h = tokio::spawn(async move{
            byte_reader.read_lines().await;
        });
        let headers = CSVReader::vec_to_string_record(options.headers.clone());
        CSVReader{
            headers,
            options,
            byte_handle: Some(h),
            line_iterator,
        }
    }

}


#[cfg(test)]
pub mod test{
    use tokio::runtime::Runtime;

    use super::*;

    fn get_runtime() -> Runtime{
        let rt = tokio::runtime::Builder::new()
            .enable_all()
            .core_threads(4)
            .threaded_scheduler()
            .enable_all().build();
        rt.unwrap()
    }

    fn get_fpath() -> &'static str{
        env!("csv_read_test_file")
    }

    #[test]
    fn should_convert_from_file(){
        let mut rt = get_runtime();
        let h = rt.spawn(async move {
            let fpath = get_fpath();
            let options_result = CSVReaderOptionsBuilder::default()
                .headers(vec![
                    "id".to_string(),
                    "first_name".to_string(),
                    "last_name".to_string(),
                    "email".to_string(),
                    "gender".to_string(),
                    "ip_address".to_string()])
                .build();
            let options = options_result.ok().unwrap();
            let mut reader = CSVReader::new(
                fpath.to_string(), options, None).await;
            let mut run = true;
            let mut i = 0;
            while run{
                let opt = reader.next().await;
                if opt.is_none(){
                    run = false;
                }else{
                    let s = opt.unwrap();
                    if s.len() >0{
                        i += s.len();
                        let hmap = s.get(0).unwrap();
                        assert!(hmap.get("id").is_some());
                        let id_opt = hmap.get("id");
                        let idn = id_opt.unwrap().parse::<u32>();
                        assert!(idn.is_ok());
                        assert!(idn.ok().unwrap() > 0);
                    }
                }
            }
            println!("{}", i);
            assert_eq!(i, 1000);
            reader.close().await;
        });
        rt.block_on(async move{
            let _r = h.await;
        })
    }
}
