//! Json line reader. Expects Nested data and has to be in a map with top level
//! keys. So `["a", "b", 1]` is not acceptable but `{"key": "a", "arr": [1,2,3]}`
//! is acceptable. The reader expects one json record per line, not one json record
//! over many lines.
//!
//! ---
//! author: Andrew Evans
//! ---

use std::collections::HashMap;

use encoding_rs::Decoder;
use serde::{Serialize, Deserialize};
use serde_json::Value;
use tokio::task::JoinHandle;

use crate::file::bytes::ByteReader;
use crate::file::iterator::LineIterator;
use crate::file::streamreader;
use crate::file::streamreader::FileLineReader;

/// Json line options
#[derive(Builder, Clone, Serialize, Deserialize)]
pub struct JsonlReaderOptions{
    #[builder(default = "100")]
    batch_size: u8,
    #[builder(default = "self.get_none_i32()")]
    pub cache_size: Option<i32>,
    #[builder(default = "self.get_none_u64()")]
    pub offset: Option<u64>
}


impl JsonlReaderOptionsBuilder{

    /// Get the builder option default, none.
    fn get_none_i32(&self) -> Option<i32>{
        None
    }

    /// Get the builder option default, none.
    fn get_none_u64(&self) -> Option<u64>{
        None
    }
}


/// Json line reader
pub struct JsonlReader{
    options: JsonlReaderOptions,
    byte_handle: Option<JoinHandle<()>>,
    line_iterator: LineIterator
}


/// Json line reader implementation
impl JsonlReader{

    /// Get the next batch from the reader
    async fn get_batch(&mut self) -> Vec<String>{
        let options = self.options.clone();
        let mut batch_vec = vec![];
        let bsize = options.batch_size;
        for _i in 0..bsize {
            let it_result = self.line_iterator.next().await;
            if it_result.is_ok(){
                let it_opt = it_result.unwrap();
                if it_opt.is_some(){
                    let record = it_opt.unwrap();
                    batch_vec.push(record);
                }else{
                    break;
                }
            }
        }
        batch_vec
    }

    /// Package the batch from the reader
    ///
    /// # Arguments
    /// * `record_strings` - Record batch vector
    fn package_batch(&mut self, record_strings: Vec<String>) -> Vec<HashMap<String, Value>>{
        let mut records: Vec<HashMap<String, Value>> = vec![];
        for record in record_strings{
            let ser_opt = serde_json::from_str(record.as_str());
            if ser_opt.is_ok(){
                let ser = ser_opt.ok().unwrap();
                records.push(ser);
            }
        }
        records
    }

    /// Get the next record if it exists. Returns None on empty. An empty
    /// vector means that processing may have failed.
    pub async fn next(&mut self) -> Option<Vec<HashMap<String, Value>>>{
        let batch_vec = self.get_batch().await;
        if batch_vec.is_empty() == false {
            let records= self.package_batch(batch_vec);
            Some(records)
        }else{
            None
        }
    }

    /// Create the iterator and bytereader for the underlying file
    ///
    /// # Arguments
    /// * `path` - Path to the file
    /// * `options` - Json line reader options
    /// * `decoder` - Optional decoder
    fn get_iterator(path: String,
                    options: JsonlReaderOptions,
                    decoder: Option<Decoder>) -> (ByteReader, LineIterator){
        let reader = FileLineReader::new(path.to_string());
        streamreader::create_iterator(
            reader, options.cache_size, decoder, options.offset)
    }

    /// Close the csv reader
    pub async fn close(&mut self){
        self.line_iterator.close().await;
        let handle_opt = self.byte_handle.take();
        if let Some(handle) = handle_opt{
            let _r = handle.await;
        }
    }

    /// Create a new Json Reader
    ///
    /// # Arguments
    /// * `path` - Path to the csv
    /// * `options` - JsonlReader Options
    /// * `decoder` - Optional decoder
    pub async fn new(
        path: String,
        options: JsonlReaderOptions,
        decoder: Option<Decoder>) -> JsonlReader{
        let (mut byte_reader, line_iterator) = JsonlReader::get_iterator(
            path, options.clone(), decoder);
        let h = tokio::spawn(async move{
            byte_reader.read_lines().await;
        });
        JsonlReader{
            options,
            byte_handle: Some(h),
            line_iterator,
        }
    }
}


#[cfg(test)]
pub mod test{
    use super::*;
    use tokio::runtime::Runtime;

    fn get_path() -> &'static str{
        env!("jsonl_test_file")
    }

    fn get_runtime() -> Runtime{
        let rt = tokio::runtime::Builder::new()
            .enable_all()
            .core_threads(4)
            .threaded_scheduler()
            .enable_all().build();
        rt.unwrap()
    }

    #[test]
    fn should_build_reader(){
        let path = get_path();
        let mut rt = get_runtime();
        let h = rt.spawn(async move {
            let name_vec = vec!["Alexa", "Deloise", "Gilbert", "May"];
            let opts_result = JsonlReaderOptionsBuilder::default().build();
            let opts = opts_result.unwrap();
            let mut reader = JsonlReader::new(path.to_string(), opts, None).await;
            let mut run = true;
            let mut i: i32 = 0;
            while run {
                let records = reader.next().await;
                if records.is_some() {
                    let rmaps = records.unwrap();
                    for rmap in rmaps {
                        assert!(rmap.len() > 0);
                        let name = rmap.get("name");
                        assert!(name.is_some());
                        let nval = name.unwrap();
                        let n = nval.as_str().unwrap();
                        assert!(name_vec.contains(&n));
                        let wopt = rmap.get("wins");
                        assert!(wopt.is_some());
                        let wval = wopt.unwrap();
                        let warr = wval.as_array();
                        assert!(warr.is_some());
                        let war_len = warr.unwrap().len();
                        assert!(vec![0,1,2].contains(&war_len));
                        i += 1;
                    }
                }else{
                    run = false;
                }
            }
            assert_eq!(i, 99);
            let _r = reader.close().await;
        });
        rt.block_on(async move{
            let _r = h.await;
        })
    }
}
